package router

import "gitea.com/jolheiser/globber"

type explanation struct {
	Includes []string `json:"includes"`
	Excludes []string `json:"excludes"`
}

type match struct {
	Line        int         `json:"line"`
	Length      int         `json:"length"`
	Include     bool        `json:"include"`
	Explanation explanation `json:"explanation"`
}

type response struct {
	Matches []match `json:"matches"`
	Error   error   `json:"error"`
}

func explain(includes, excludes []globber.Globber) explanation {
	expin := make([]string, 0)
	for _, inc := range includes {
		expin = append(expin, inc.Pattern)
	}

	expex := make([]string, 0)
	for _, exp := range excludes {
		expex = append(expex, "!"+exp.Pattern)
	}

	return explanation{
		Includes: expin,
		Excludes: expex,
	}
}
