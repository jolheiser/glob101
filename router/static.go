package router

import (
	"net/http"

	"github.com/markbates/pkger"
)

func css() http.Handler {
	return http.StripPrefix("/css/", http.FileServer(pkger.Dir("/static/css/")))
}

func js() http.Handler {
	return http.StripPrefix("/js/", http.FileServer(pkger.Dir("/static/js/")))
}
