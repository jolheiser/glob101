package router

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/go-chi/chi"
	"github.com/markbates/pkger"

	"gitea.com/jolheiser/beaver"
	"gitea.com/jolheiser/globber"
)

var indexTpl = pkger.Include("/static/templates/index.tmpl")

func New() *chi.Mux {
	r := chi.NewRouter()

	r.Route("/", func(r chi.Router) {
		r.Get("/", serveGET)
		r.Post("/", servePOST)
	})

	r.Handle("/css/*", css())
	r.Handle("/js/*", js())

	return r
}

func serveGET(res http.ResponseWriter, req *http.Request) {
	fi, err := pkger.Open(indexTpl)
	if err != nil {
		beaver.Error(err)
		return
	}

	indexHTML, err := ioutil.ReadAll(fi)
	if err != nil {
		beaver.Error(err)
		return
	}

	_, _ = res.Write(indexHTML)
}

func servePOST(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Content-Type", "application/json")
	dotGlobber := req.FormValue("globber")
	tests := req.FormValue("tests")

	set, err := globber.ParseString(dotGlobber, '/')
	if err != nil {
		resp := response{Error: err}
		content, _ := json.Marshal(resp)
		_, _ = res.Write(content)
		return
	}

	resp := response{
		Matches: make([]match, 0),
	}
	for idx, test := range strings.Split(tests, "\n") {
		test = strings.TrimSpace(test)
		if test == "" {
			continue
		}

		includes, excludes := set.Explain(test)
		if len(includes)+len(excludes) > 0 {
			resp.Matches = append(resp.Matches, match{
				Line:        idx,
				Length:      len(test),
				Include:     len(excludes) == 0,
				Explanation: explain(includes, excludes),
			})
		}
	}
	content, _ := json.Marshal(resp)
	_, _ = res.Write(content)
}
