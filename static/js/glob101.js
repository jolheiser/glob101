// AJAX function for async call
function ajax(method, url, data, callback) {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState === XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
            callback(xmlhttp.status, xmlhttp.responseText);
        }
    };
    xmlhttp.open(method, url, true);
    xmlhttp.send(data);
}

// Globber (top) CodeMirror instance
const globber = CodeMirror.fromTextArea(document.getElementById("globber"), {
    lineWrapping: true,
    lineNumbers: true,
});

// Tests (bottom) CodeMirror instance
const tests = CodeMirror.fromTextArea(document.getElementById("tests"), {
    lineWrapping: true,
    lineNumbers: true,
});

// Monitor input changes and refresh results as needed
let count = 0;
let gv, tv, gvLast, tvLast, lock;
let markers = [];
setInterval(() => {
    gv = globber.getDoc().getValue();
    tv = tests.getDoc().getValue();
    if (gv.trim() === "" || tv.trim() === "") return;
    if (gv !== gvLast || tv !== tvLast) {
        gvLast = gv;
        tvLast = tv;
        count = 0;
        lock = false;
        return;
    }
    if (lock) return;
    count++;

    if (count >= 5) {
        for (const marker of markers) {
            marker.clear();
        }
        markers = [];
        count = 0;
        lock = true;
        const form = new FormData();
        form.set("globber", gv);
        form.set("tests", tv);
        ajax("POST", "/", form, (status, resp) => {
            if (status === 200) {
                const data = JSON.parse(resp);
                for (let i = 0; i < data.matches.length; i++) {
                    const match = data.matches[i];
                    let color = "lightpink";
                    if (match.include) {
                        color = "lightgreen";
                    }
                    const className = `marker-${i}`;
                    markers.push(tests.getDoc().markText({line: match.line, ch: 0}, {line: match.line, ch: match.length}, {className: className, css: `background: ${color};`}));

                    const includes = match.explanation.includes;
                    if (includes.length) {
                        tippy(`.${className}`, {
                            placement: 'top',
                            boundary: 'window',
                            flip: false,
                            multiple: true,
                            content: `<p><strong>Included By</strong></p><p>${includes.join('</p><p>')}</p>`,
                        });
                    }

                    const excludes = match.explanation.excludes;
                    if (excludes.length) {
                        tippy(`.${className}`, {
                            placement: 'bottom',
                            boundary: 'window',
                            flip: false,
                            multiple: true,
                            content: `<p><strong>Excluded By</strong></p><p>${excludes.join('</p><p>')}</p>`,
                        })
                    }
                }
            }
        });
    }
}, 200);

// Example button/link
document.getElementById("example").addEventListener('click', () => {
    globber.getDoc().setValue(exampleGlobber);
    tests.getDoc().setValue(exampleTests);
});

const exampleGlobber = `# All .go files
**.go

# Files starting with go, but ONLY in the base directory
go*

# Files starting with go, in ANY directory
**/go*

# All docs
docs/*

# But not RST docs
!docs/*.rst

# Exclude files with a '3' in their name (e.g. found3.go or text/file3.txt)
!**3*`;

const exampleTests = `docs/docs.md
docs/docs.rst
docs/docs.txt

misc/go.misc
misc/misc.go

text/file1.txt
text/file2.txt
text/file3.txt

found1.go
found2.go
found3.go

go-found1.txt
go-found2.md

missed1.og
missed2.og

og-missed1.py
og-missed2.java`;