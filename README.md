# Glob101
A **very** simple web UI for testing globs with [Globber](https://gitea.com/jolheiser/globber).

## Building and Running
```
go get -u gitea.com/jolheiser/glob101
go build
./glob101
```

### Screenshot
![screenshot](screenshot.png)