module gitea.com/jolheiser/glob101

go 1.13

require (
	gitea.com/jolheiser/beaver v1.0.0
	gitea.com/jolheiser/globber v0.0.1
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/markbates/pkger v0.14.0
	github.com/urfave/cli/v2 v2.1.1
)
