package main

import (
	"gitea.com/jolheiser/beaver/color"
	"os"

	"github.com/urfave/cli/v2"

	"gitea.com/jolheiser/beaver"
	"gitea.com/jolheiser/glob101/cmd"
)

var Version = "development"

func main() {
	beaver.Console.Format = beaver.FormatOptions{
		TimePrefix:  true,
		StackPrefix: true,
		StackLimit:  20,
		LevelPrefix: true,
		LevelColor:  true,
	}
	color.Time = color.New(color.FgCyan)
	color.Stack = color.New(color.FgGreen)

	app := cli.App{
		Name:    "Glob101",
		Version: Version,
		Usage:   "Web application for testing globs",
		Commands: []*cli.Command{
			cmd.Web,
		},
	}

	if err := app.Run(os.Args); err != nil {
		beaver.Fatal(err)
	}
}
