package cmd

import (
	"fmt"
	"net/http"

	"github.com/urfave/cli/v2"

	"gitea.com/jolheiser/beaver"
	"gitea.com/jolheiser/glob101/router"
)

var (
	Web = &cli.Command{
		Name:    "web",
		Aliases: []string{"w"},
		Usage:   "Launch the web server",
		Action:  runWeb,
		Flags: []cli.Flag{
			&cli.IntFlag{
				Name:        "port",
				Aliases:     []string{"p"},
				Usage:       "The port to run on",
				Value:       5000,
				Destination: &portFlag,
			},
		},
	}
	portFlag int
)

func runWeb(ctx *cli.Context) error {
	port := fmt.Sprintf(":%d", portFlag)
	beaver.Infof("Running on http://localhost%s", port)
	if err := http.ListenAndServe(port, router.New()); err != nil {
		return err
	}
	return nil
}
